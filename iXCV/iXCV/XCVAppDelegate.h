//
//  XCVAppDelegate.h
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XCVViewController;

@interface XCVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) XCVViewController *viewController;

@end
