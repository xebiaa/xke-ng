//
//  XCVLocation.h
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XCVLocation : NSObject

@property (nonatomic, assign) NSInteger id;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) NSInteger capacity;

@end
