//
//  XCVViewController.m
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "XCVViewController.h"

@interface XCVViewController ()

@end

@implementation XCVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
