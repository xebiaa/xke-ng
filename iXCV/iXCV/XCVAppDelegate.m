//
//  XCVAppDelegate.m
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <RestKit/RestKit.h>

#import "XCVAppDelegate.h"

#import "XCVViewController.h"

#import "XCVCredential.h"
#import "XCVSession.h"
#import "XCVConference.h"

@implementation XCVAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    RKLogConfigureByName("RestKit/Network*", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    
    // Initialize RestKit
	RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://xke.xebia.com/xkeng"]];
    
    // Enable automatic network activity indicator management
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // Setup our object mappings
    RKObjectMapping *sessionMapping = [RKObjectMapping requestMapping];
    [sessionMapping addAttributeMappingsFromDictionary:@{
     @"id": @"sessionID",
     @"title": @"title",
     @"description": @"description",
     @"startTime": @"startTime",
     @"endTime": @"endTime",
     @"limit": @"limit",
     @"type": @"type",
     }];
    [objectManager addRequestDescriptor:
     [RKRequestDescriptor requestDescriptorWithMapping:sessionMapping
                                           objectClass:[XCVSession class]
                                           rootKeyPath:@"session"]];
    
    RKObjectMapping *conferenceMapping = [RKObjectMapping requestMapping];
    [conferenceMapping addAttributeMappingsFromDictionary:@{
     @"id": @"conferenceID",
     @"title": @"title",
     @"begin": @"begin",
     @"end": @"end",
     }];
    [objectManager addRequestDescriptor:
     [RKRequestDescriptor requestDescriptorWithMapping:conferenceMapping
                                           objectClass:[XCVConference class]
                                           rootKeyPath:@"conferences"]];
    
    RKRelationshipMapping *conferenceRelationship =
    [RKRelationshipMapping relationshipMappingFromKeyPath:@"sessions"
                                                toKeyPath:@"session"
                                              withMapping:sessionMapping];
    
    [conferenceMapping addPropertyMapping:conferenceRelationship];
    
    // Update date format so that we can parse Twitter dates properly
	// Wed Sep 29 15:31:08 +0000 2010
    [RKObjectMapping addDefaultDateFormatterForString:@"E MMM d HH:mm:ss Z y" inTimeZone:nil];
    
    // Uncomment these lines to use XML, comment it to use JSON
    //    objectManager.acceptMIMEType = RKMIMETypeXML;
    //    statusMapping.rootKeyPath = @"statuses.status";
    
    // Register our mappings with the provider using a resource path pattern

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[XCVViewController alloc] initWithNibName:@"XCVViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
    
    //TODO store cookie in cookie store:     [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie]; 
    //TODO: https://github.com/RestKit/RKGithub/tree/master/Code
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
