//
//  XCVAuthor.h
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XCVAuthor : NSObject

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *mail;
@property (nonatomic, copy) NSString *name;


@end
