//
//  XCVCredential.h
//  iXCV
//
//  Created by Jeroen Leenarts on 05-05-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XCVCredential : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *password;

@end
