//
//  main.m
//  iXCV
//
//  Created by Jeroen Leenarts on 24-04-12.
//  Copyright (c) 2012 Xebia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XCVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XCVAppDelegate class]));
    }
}
