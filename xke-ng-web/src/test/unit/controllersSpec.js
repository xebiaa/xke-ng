describe('In controllers', function() {

	 beforeEach(function(){
		this.addMatchers({
			toEqualData: function(expected) {
				return angular.equals(this.actual, expected);
			}
		});
	});
		 
 
	describe('ConferenceController', function() {
		var scope, ctrl, $httpBackend, location;

		var data = {
		  "id":"5107fe130364ed385e935c79",
		  "title":"Test-XKE",
		  "begin":"2013-02-05T16:00:23.289+01:00",
		  "end":"2013-02-05T20:00:23.289+01:00"
	  };	
		 
		beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, $location) {
			$httpBackend = _$httpBackend_;
      $httpBackend.expectGET('/xkeng/conference/5107fe130364ed385e935c79').respond(data);

			scope = $rootScope.$new();

      location = $location;

			ctrl = $controller(ConferenceController, {$scope: scope, $routeParams: {id: "5107fe130364ed385e935c79"}});
		}));

		it('should get the conference by id from the service', function() {
			expect(scope.conference).toEqualData({});
			$httpBackend.flush();

			expect(scope.conference).toEqualData(data);
		});

    it('should remove the conference when asked to', function() {
      // for the get
      $httpBackend.flush();

      // for the delete
      $httpBackend.expectDELETE('/xkeng/conference/5107fe130364ed385e935c79').respond({});

      scope.remove();

      expect(location.path()).toBe('');

      $httpBackend.flush();

      expect(location.path()).toBe('/conferences');
    });
	});

	describe('ConferencesController', function(){
		var scope, ctrl, $httpBackend;

		var data = [{
		  "id":"5107fe130364ed385e935c79",
		  "title":"Test-XKE",
		  "begin":"2013-02-05T16:00:23.289+01:00",
		  "end":"2013-02-05T20:00:23.289+01:00"
		}];	
		 
		beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
			$httpBackend = _$httpBackend_;
			$httpBackend.expectGET('/xkeng/conferences/summary/pastcount/0/futurecount/10').
			respond(data);
		 
			scope = $rootScope.$new();

			ctrl = $controller(ConferencesController, {$scope: scope});
		}));
		 
		 
		it('should get conferences from backend', function() {
			expect(scope.conferences).toEqual([]);
			$httpBackend.flush();
		 
			expect(scope.conferences).toEqualData(data);
		});	

	 	it('should remove the conference when asked to', function() {
      // for the delete
      $httpBackend.expectDELETE('/xkeng/conference/5107fe130364ed385e935c79').respond({});
			$httpBackend.expectGET('/xkeng/conferences/summary/pastcount/0/futurecount/10').respond(data);
      
      scope.removeConference('5107fe130364ed385e935c79');

      $httpBackend.flush();

    }); 		
	});

	beforeEach(module('ConferenceServices'));

});