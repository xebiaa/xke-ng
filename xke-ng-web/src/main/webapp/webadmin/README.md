XKE Next Generation WebAdmin (HTML5 Client)
===========================================



This sub-project realizes the WebAdmin HTML5 client for XKE Next Generation
(XKE-NG for short).

[HTML5 Client wiki on github][1]

[1]: <https://github.com/xebia/xke-ng/wiki/Html5-client>

Use http://localhost:8080/xkeng/webadmin/index.html to access the webadmin.

Technologies used:

-   AngularJS - http://angularjs.org/

-   AngularStrap - http://mgcrea.github.com/angular-strap/

-   Jasmine -  http://pivotal.github.com/jasmine/

-   Bootstrap - http://twitter.github.com/bootstrap/

-   XKE-NG Rest api


