angular.module('xkeng-webadmin', ['ConferenceServices', '$strap.directives']).
	config(['$routeProvider', function($routeProvider) {
	$routeProvider.
		when('/conferences', {templateUrl: 'partials/conference-list.html', controller: ConferencesController}).
    	//when('/conferences', {templateUrl: 'partials/conference-list.html', controller: LocationsController}).
		when('/conferences/:id', {templateUrl: 'partials/conference.html', controller: ConferenceController}).
		otherwise({redirectTo: '/conferences'});
}]);
