angular.module('ConferenceServices', ['ngResource']).
	factory('Conferences', function($resource) {
 		return $resource('/xkeng/conferences/summary/pastcount/10/futurecount/10', {}, {
 			query: {method:'GET', params:{}, isArray:true}
 		});
 	}).
 	factory('Conference', function($resource) {
 		return $resource('/xkeng/conference/:id', {}, {
 			query: {method:'GET', params:{id:''}, isArray:true},
      		save: {method:'POST', isArray:false},
      		update: {method:'PUT', params:{id:''}, isArray:false},
      		remove: {method:'DELETE', params:{id:''}, isArray:true}
      	});
 	}).
    factory('Locations', function($resource) {
        return $resource('/xkeng/locations', {}, {
            query: {method:'GET', params:{}, isArray:true}
        });
  }).
  factory('Slots', function($resource) {
      return $resource('/xkeng/conference/:id/slots');
  });
;