function ConferencesController($scope, $location, Conferences, Conference, Slots) {
	$scope.conferences = Conferences.query();

 	$scope.addConference = function() {
 		$location.path('/conferences/:null');
	}

	$scope.removeConference = function(conferenceId) {
		Conference.remove({id:conferenceId}, function () {
      $scope.conferences = Conferences.query();
    });
  }

    $scope.loadConferenceDetails = function(conferenceId) {
        $scope.conference = Conference.get({id:conferenceId});
        $scope.slots = Slots.get({id:conferenceId});
    }
}

function ConferenceController($scope, $routeParams, $location, Conference) {
  var updateDateAndTimestamps = function(conference) {
    $scope.conferenceBeginDate = getFormattedDate(conference.begin, 'dd-mm-yyyy');
    $scope.conferenceBeginTime = getFormattedDate(conference.begin, 'HH:MM');

    $scope.conferenceEndDate = getFormattedDate(conference.end, 'dd-mm-yyyy');
    $scope.conferenceEndTime = getFormattedDate(conference.end, 'HH:MM');
  }
  
// unclear to me why the if statement worked priviously and currently not anymore. It causes the $scope.save method to an exception
// saying that the $scope.conference.$save method does not exist. The Conference.get(...) method causes an 404 in case there is no
// id.
  if ($routeParams.id != ':null') {
	  $scope.conference = Conference.get({id: $routeParams.id}, function() {
	    updateDateAndTimestamps($scope.conference);
  	});
	} else {
    $scope.conference = new Conference;
  }

  $scope.save = function() {
    var beginStr = fromDutchFormat($scope.conferenceBeginDate).format('yyyy-mm-dd') + "T" + $scope.conferenceBeginTime + ":00.00";
    var begin = toDate(beginStr);
    $scope.conference.begin = begin;

    var endStr = fromDutchFormat($scope.conferenceEndDate).format('yyyy-mm-dd') + "T" + $scope.conferenceEndTime + ":00.00";
    var end = toDate(endStr);
    $scope.conference.end = end;

//    $scope.json = angular.toJson($scope.conference);

    if ($routeParams.id != ':null') {
      $scope.conference.$update({id: $scope.conference.id}, function (conference) {
       updateDateAndTimestamps(conference);
      });
		} else {
      $scope.conference.$save(function (conference) {
       updateDateAndTimestamps(conference);
      });
    }

		$location.path('/conferences');
  } 

  $scope.remove = function() {
    $scope.conference.$remove({id: $scope.conference.id}, function() {
		  $location.path('/conferences');
    });
	}

}

function LocationsController($scope, Locations) {
    $scope.locations = Locations.query();
    $scope.locations.sort();
}
