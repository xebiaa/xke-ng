# XKE-NG REST API

This document describes the REST API of the XKE-NG platform.

## JSON Calls

**Login**

> POST /xkeng/login

```json
{
   "username": "upeter",
   "encryptedPassword": "716AF9A87BD5A9735C20AC8FCED05F40"
}
```

**Logout**

> GET /xkeng/logout

## Conference

**Query next available slot**

> GET /xkeng/conference/next/1/slots


**New conference Conference**

> POST /xkeng/conference

```json
{
    "id": "51101a667d1338f9136ba47e",
    "title": "XKE",
    "begin": "2013-02-07T21:30:30.122+01:00",
    "end": "2013-02-08T01:30:30.122+01:00",
    "sessions": [],
    "locations": [
        {
            "id": -1494802312,
            "description": "Maup",
            "capacity": 20
        },
        {
            "id": -1494802307,
            "description": "Laap",
            "capacity": 30
        }
    ]
}
```

**Update conference**

> PUT /xkeng/conference/{conferenceId}

> example /xkeng/conference/51101a667d1338f9136ba47e

```json
{
    "id": "51101a667d1338f9136ba47e",
    "title": "XKENG",
    "begin": "2013-02-07T21:30:30.122+01:00",
    "end": "2013-02-08T01:30:30.122+01:00",
    "sessions": [],
    "locations": [
        {
            "id": -1494802312,
            "description": "Maup",
            "capacity": 20
        },
        {
            "id": -1494802307,
            "description": "Laap",
            "capacity": 30
        }
    ]
}
```

**Query added/updated conference**
> GET /xkeng/conference/{conferenceId}

> example /xkeng/conference/51101a667d1338f9136ba47e

**Delete conference**
> DELETE /xkeng/conference/{conferenceId}

> example /xkeng/conference/51101a667d1338f9136ba47e

**Query conference slots**

> GET /xkeng/conference/{conferenceId}/slots

> example /xkeng/conference/51101a667d1338f9136ba47e/slots

```json
{
    "id": 1360009832492,
    "title": "Scala rocks even more title changed!",
    "description": "Scala is a scalable programming language",
    "startTime": "2013-02-07T21:30:30.122+01:00",
    "endTime": "2013-02-07T22:30:30.122+01:00",
    "limit": "20 people",
    "type": "STRATEGIC",
    "authors": [
        {
            "userId": "peteru",
            "mail": "upeter@xebia.com",
            "name": "Urs Peter"
        },
        {
            "userId": "amooy",
            "mail": "amooy@xebia.com",
            "name": "Age Mooy"
        }
    ],
    "comments": [
        {
            "user": "udummy",
            "comment": "bla bla comment"
        }
    ],
    "ratings": [
        {
            "user": "udummy",
            "rate": 10
        }
    ],
    "labels": [
        "Scala",
        "DSL"
    ],
    "location": {
        "id": -1494802307,
        "description": "Laap",
        "capacity": 30
    }
}
```

## Session

**Add session to conference**

> POST /xkeng/conference/{conferenceId}/session

> example /xkeng/conference/51101a667d1338f9136ba47e/session

```json
{
    "id": 1360009830554,
    "title": "Scala rocks even more",
    "description": "Scala is a scalable programming language",
    "startTime": "2013-02-07T21:30:30.122+01:00",
    "endTime": "2013-02-07T22:30:30.122+01:00",
    "limit": "20 people",
    "type": "STRATEGIC",
    "authors": [
        {
            "userId": "peteru",
            "mail": "upeter@xebia.com",
            "name": "Urs Peter"
        },
        {
            "userId": "amooy",
            "mail": "amooy@xebia.com",
            "name": "Age Mooy"
        }
    ],
    "comments": [],
    "ratings": [],
    "labels": [
        "Scala",
        "DSL"
    ],
    "location": {
        "id": -1494802307,
        "description": "Laap",
        "capacity": 30
    }
}
```

**Update session**

> PUT /xkeng/session/{sessionId}

> example /xkeng/session/1360009832492

```json
{
    "id": 1360009832492,
    "title": "Scala rocks even more title changed!",
    "description": "Scala is a scalable programming language",
    "startTime": "2013-02-07T21:30:30.122+01:00",
    "endTime": "2013-02-07T22:30:30.122+01:00",
    "limit": "20 people",
    "type": "STRATEGIC",
    "authors": [
        {
            "userId": "peteru",
            "mail": "upeter@xebia.com",
            "name": "Urs Peter"
        },
        {
            "userId": "amooy",
            "mail": "amooy@xebia.com",
            "name": "Age Mooy"
        }
    ],
    "comments": [],
    "ratings": [],
    "labels": [
        "Scala",
        "DSL"
    ],
    "location": {
        "id": -1494802307,
        "description": "Laap",
        "capacity": 30
    }
}
```

**Query session**

> GET /xkeng/session/{sessionId}

> example /xkeng/session/1360009832492

## Rating

**Add rating to session**

> POST /xkeng/feedback/{sessionId}/rating

> example /xkeng/feedback/1360009832492/rating

```json
{"rate":10}
```
## Comment

**Add comment to session**

> POST /xkeng/feedback/{sessionId}/comment

> example: /xkeng/feedback/1360009832492/comment

```json
{"comment":"bla bla comment"}
```

**Delete session**

> DELETE /xkeng/session/{sessionId}

> example /xkeng/session/1360009832492

## Labels

**Query labels**

> GET /xkeng/labels

```
["Functional Programming","Scala","Middleware","Database","Javascript","DSL","Beauty","Functions","Quick","Deoployment","Mongo"]
```

**Query labels by author**

> GET /xkeng/labels/author/amooy

```
["Functional Programming","Scala","Middleware","DSL","Beauty","Functions","Quick","Deoployment"]
```
